﻿using ControlAcceso.Data;
using ControlAcceso.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Controllers
{
    public class AdminDteosController:Controller
    {
        private readonly ControlContext _context;

        public AdminDteosController(ControlContext context)
        {
            _context = context;
        }


        // GET: AdminDteos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: AdminDteos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,Email,Active,Name,Surname,Phone,Age")] AdminDto adminDto)
        {
            Role role = _context.Role.Where(r => r.RoleName == "Admin").FirstOrDefault();

            if (ModelState.IsValid)
            {
               
                UserAccount userAccount = new UserAccount();
                Admin admin = new Admin();

                userAccount.Username = adminDto.Username;
                userAccount.Password = Util.Encriptar(adminDto.Password);
                userAccount.Email = adminDto.Email;
                userAccount.RoleID = role.ID;
                userAccount.Active = adminDto.Active;

                _context.Add(userAccount);
                await _context.SaveChangesAsync();

                admin.Name = adminDto.Name;
                admin.Surname = adminDto.Surname;
                admin.Phone = adminDto.Phone;
                admin.Age = adminDto.Age;
                admin.UserAccountID = userAccount.ID;
                _context.Add(admin);

                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Admins");
            }
            return View(adminDto);
        }




        private bool AdminDtoExists(int id)
        {
            return _context.Admin.Any(e => e.ID == id);
        }
    }
}
