﻿using ControlAcceso.Data;
using ControlAcceso.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Controllers
{
    public class CustomerDteosController:Controller
    {
        private readonly ControlContext _context;

        public CustomerDteosController(ControlContext context)
        {
            _context = context;
        }

        // GET: CustomerDteos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: CustomerDteos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,Email,Active,Name,Surname,Phone,Age,Birthday,DNI,Address,City,PostalCode,Level")] CustomerDto customerDto)
        {
            Role role = _context.Role.Where(r => r.RoleName == "Customer").FirstOrDefault();
            if (ModelState.IsValid)
            {

               
                UserAccount userAccount = new UserAccount();
                Customer customer = new Customer();


                userAccount.Username = customerDto.Username;
                userAccount.Password = Util.Encriptar(customerDto.Password);
                userAccount.Email = customerDto.Email;
                userAccount.RoleID = role.ID;
                userAccount.Active = customerDto.Active;

                _context.Add(userAccount);
                await _context.SaveChangesAsync();

                customer.Name = customerDto.Name;
                customer.Surname = customerDto.Surname;
                customer.Phone = customerDto.Phone;
                customer.Age = customerDto.Age;
                customer.UserAccountID = userAccount.ID;
                customer.Birthday = customerDto.Birthday;
                customer.DNI = customerDto.DNI;
                customer.Address = customerDto.Address;
                customer.City = customerDto.City;
                customer.PostalCode = customerDto.PostalCode;
                customer.Level = customerDto.Level;
                _context.Add(customer);

                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Customers");
            }
            return View(customerDto);
        }




    }
}
