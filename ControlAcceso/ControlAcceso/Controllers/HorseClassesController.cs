﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ControlAcceso.Data;
using ControlAcceso.Models;

namespace ControlAcceso.Controllers
{
    public class HorseClassesController : Controller
    {
        private readonly ControlContext _context;

        public HorseClassesController(ControlContext context)
        {
            _context = context;
        }

        // GET: HorseClasses
        public async Task<IActionResult> Index()
        {
            var controlContext = _context.HorseClass.Include(h => h.RaceTrack).Include(h => h.Teacher);
            return View(await controlContext.ToListAsync());
        }

        // GET: HorseClasses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horseClass = await _context.HorseClass
                .Include(h => h.RaceTrack)
                .Include(h => h.Teacher)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (horseClass == null)
            {
                return NotFound();
            }

            return View(horseClass);
        }

        // GET: HorseClasses/Create
        public IActionResult Create()
        {
            ViewData["RaceTrackID"] = new SelectList(_context.RaceTrack, "ID", "Name");
            ViewData["TeacherID"] = new SelectList(_context.Teacher, "ID", "Name");
            return View();
        }

        // POST: HorseClasses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Level,Hour,Date,Assistants,Duration,TeacherID,RaceTrackID")] HorseClass horseClass)
        {
            if (ModelState.IsValid)
            {
                _context.Add(horseClass);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["RaceTrackID"] = new SelectList(_context.RaceTrack, "ID", "Name", horseClass.RaceTrackID);
            ViewData["TeacherID"] = new SelectList(_context.Teacher, "ID", "Name", horseClass.TeacherID);
            return View(horseClass);
        }

        // GET: HorseClasses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horseClass = await _context.HorseClass.FindAsync(id);
            if (horseClass == null)
            {
                return NotFound();
            }
            ViewData["RaceTrackID"] = new SelectList(_context.RaceTrack, "ID", "Name", horseClass.RaceTrackID);
            ViewData["TeacherID"] = new SelectList(_context.Teacher, "ID", "Name", horseClass.TeacherID);
            return View(horseClass);
        }

        // POST: HorseClasses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Level,Hour,Date,Assistants,Duration,TeacherID,RaceTrackID")] HorseClass horseClass)
        {
            if (id != horseClass.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(horseClass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HorseClassExists(horseClass.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RaceTrackID"] = new SelectList(_context.RaceTrack, "ID", "Name", horseClass.RaceTrackID);
            ViewData["TeacherID"] = new SelectList(_context.Teacher, "ID", "Name", horseClass.TeacherID);
            return View(horseClass);
        }

        // GET: HorseClasses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horseClass = await _context.HorseClass
                .Include(h => h.RaceTrack)
                .Include(h => h.Teacher)
                .FirstOrDefaultAsync(m => m.ID == id);
            if (horseClass == null)
            {
                return NotFound();
            }

            return View(horseClass);
        }

        // POST: HorseClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var horseClass = await _context.HorseClass.FindAsync(id);
            _context.HorseClass.Remove(horseClass);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HorseClassExists(int id)
        {
            return _context.HorseClass.Any(e => e.ID == id);
        }
    }
}
