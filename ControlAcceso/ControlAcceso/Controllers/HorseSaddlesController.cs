﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ControlAcceso.Data;
using ControlAcceso.Models;

namespace ControlAcceso.Controllers
{
    public class HorseSaddlesController : Controller
    {
        private readonly ControlContext _context;

        public HorseSaddlesController(ControlContext context)
        {
            _context = context;
        }

        // GET: HorseSaddles
        public async Task<IActionResult> Index()
        {
            return View(await _context.HorseSaddle.ToListAsync());
        }

        // GET: HorseSaddles/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horseSaddle = await _context.HorseSaddle
                .FirstOrDefaultAsync(m => m.ID == id);
            if (horseSaddle == null)
            {
                return NotFound();
            }

            return View(horseSaddle);
        }

        // GET: HorseSaddles/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: HorseSaddles/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name,Condition,Reserved")] HorseSaddle horseSaddle)
        {
            if (ModelState.IsValid)
            {
                _context.Add(horseSaddle);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(horseSaddle);
        }

        // GET: HorseSaddles/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horseSaddle = await _context.HorseSaddle.FindAsync(id);
            if (horseSaddle == null)
            {
                return NotFound();
            }
            return View(horseSaddle);
        }

        // POST: HorseSaddles/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name,Condition,Reserved")] HorseSaddle horseSaddle)
        {
            if (id != horseSaddle.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(horseSaddle);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!HorseSaddleExists(horseSaddle.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(horseSaddle);
        }

        // GET: HorseSaddles/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var horseSaddle = await _context.HorseSaddle
                .FirstOrDefaultAsync(m => m.ID == id);
            if (horseSaddle == null)
            {
                return NotFound();
            }

            return View(horseSaddle);
        }

        // POST: HorseSaddles/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var horseSaddle = await _context.HorseSaddle.FindAsync(id);
            _context.HorseSaddle.Remove(horseSaddle);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool HorseSaddleExists(int id)
        {
            return _context.HorseSaddle.Any(e => e.ID == id);
        }
    }
}
