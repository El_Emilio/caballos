﻿using ControlAcceso.Data;
using ControlAcceso.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace ControlAcceso.Controllers
{
    public class LoginController : Controller
    {

        private readonly ControlContext _context;

        public LoginController(ControlContext context)
        {
            _context = context;
        }

        //Get de Login
        public IActionResult Index()
        {
            return View();
        }
        //Post
        public IActionResult Login(string username, string password)
        {

            string passwordByte = Util.Encriptar(password);
            Debug.WriteLine(username + "------------------------------- " + password);                                                                      //Comparamos la pass encriptada
            var usuario = _context.UserAccounts.Where(u => u.Username == username && u.Password == passwordByte).FirstOrDefault();

            if (usuario != null)
            {
                HttpContext.Session.SetString("usuario", usuario.ID.ToString());
                HttpContext.Session.SetString("username", usuario.Username.ToString());
                var rol = _context.Role.Where(r => r.ID.Equals(usuario.RoleID)).FirstOrDefault();
                HttpContext.Session.SetString("rol", rol.RoleName.ToString());


                Util.Menus = _context.RoleHasMenu.Include(m => m.Menu).Where(m => m.RoleID == usuario.RoleID).Select(m => m.Menu).ToList();
            }

            return RedirectToAction("Index", "Home");

        }

        public IActionResult Logout()
        {
            HttpContext.Session.Remove("usuario");
            HttpContext.Session.Remove("username");
            HttpContext.Session.Remove("rol");
            Util.Menus = new List<Menu>();
            return RedirectToAction("Index", "Login");
        }
    }
}
