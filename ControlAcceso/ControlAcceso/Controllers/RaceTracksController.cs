﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ControlAcceso.Data;
using ControlAcceso.Models;

namespace ControlAcceso.Controllers
{
    public class RaceTracksController : Controller
    {
        private readonly ControlContext _context;

        public RaceTracksController(ControlContext context)
        {
            _context = context;
        }

        // GET: RaceTracks
        public async Task<IActionResult> Index()
        {
            return View(await _context.RaceTrack.ToListAsync());
        }

        // GET: RaceTracks/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var raceTrack = await _context.RaceTrack
                .FirstOrDefaultAsync(m => m.ID == id);
            if (raceTrack == null)
            {
                return NotFound();
            }

            return View(raceTrack);
        }

        // GET: RaceTracks/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: RaceTracks/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Name")] RaceTrack raceTrack)
        {
            if (ModelState.IsValid)
            {
                _context.Add(raceTrack);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(raceTrack);
        }

        // GET: RaceTracks/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var raceTrack = await _context.RaceTrack.FindAsync(id);
            if (raceTrack == null)
            {
                return NotFound();
            }
            return View(raceTrack);
        }

        // POST: RaceTracks/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Name")] RaceTrack raceTrack)
        {
            if (id != raceTrack.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(raceTrack);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RaceTrackExists(raceTrack.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(raceTrack);
        }

        // GET: RaceTracks/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var raceTrack = await _context.RaceTrack
                .FirstOrDefaultAsync(m => m.ID == id);
            if (raceTrack == null)
            {
                return NotFound();
            }

            return View(raceTrack);
        }

        // POST: RaceTracks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var raceTrack = await _context.RaceTrack.FindAsync(id);
            _context.RaceTrack.Remove(raceTrack);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RaceTrackExists(int id)
        {
            return _context.RaceTrack.Any(e => e.ID == id);
        }
    }
}
