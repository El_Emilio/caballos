﻿using ControlAcceso.Data;
using ControlAcceso.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Controllers
{
    public class TeacherDteosController: Controller
    {
        private readonly ControlContext _context;

        public TeacherDteosController(ControlContext context)
        {
            _context = context;
        }



        // GET: TeacherDteos/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TeacherDteos/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,Username,Password,Email,Active,Name,Surname,Phone,Age")] TeacherDto teacherDto)
        {
            Role role = _context.Role.Where(r => r.RoleName == "Teacher").FirstOrDefault();
            if (ModelState.IsValid)
            {
                
                Teacher teacher = new Teacher();
                UserAccount userAccount = new UserAccount();

                
                userAccount.Username = teacherDto.Username;
                userAccount.Password = Util.Encriptar(teacherDto.Password);
                userAccount.Email = teacherDto.Email;
                userAccount.RoleID = role.ID;
                userAccount.Active = teacherDto.Active;

                _context.Add(userAccount);
                await _context.SaveChangesAsync();

                teacher.Name = teacherDto.Name;
                teacher.Surname = teacherDto.Surname;
                teacher.Phone = teacherDto.Phone;
                teacher.Age = teacherDto.Age;
                teacher.UserAccountID = userAccount.ID;
                _context.Add(teacher);


                await _context.SaveChangesAsync();
                return RedirectToAction("Index", "Teachers");
            }
            return View(teacherDto);
        }


    }
}
