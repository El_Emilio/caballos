﻿using ControlAcceso.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;



namespace ControlAcceso.Data
{
    public class ControlContext:DbContext
    {

        public ControlContext(DbContextOptions<ControlContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Menu>().ToTable("Menu");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<RoleHasMenu>().ToTable("RoleHasMenu");
            modelBuilder.Entity<Admin>().ToTable("Admin");
            modelBuilder.Entity<Customer>().ToTable("Customer");
            modelBuilder.Entity<Teacher>().ToTable("Teacher");
            modelBuilder.Entity<Horse>().ToTable("Horse");
            modelBuilder.Entity<HorseClass>().ToTable("HorseClass");
            modelBuilder.Entity<HorseSaddle>().ToTable("HorseSaddle");
            modelBuilder.Entity<Menu>().ToTable("Menu");
            modelBuilder.Entity<RaceTrack>().ToTable("RaceTrack");
            modelBuilder.Entity<UserAccount>().ToTable("UserAccount");

        }

        public DbSet<UserAccount> UserAccounts { get; set; }
        public DbSet<Menu> Menu { get; set; }
        public DbSet<Role> Role { get; set; }
        public DbSet<RoleHasMenu> RoleHasMenu { get; set; }
        public DbSet<Admin> Admin { get; set; }
        public DbSet<Customer> Customer { get; set; }

        public DbSet<Booking> Booking { get; set; }
        public DbSet<Horse> Horse { get; set; }
        public DbSet<ControlAcceso.Models.HorseClass> HorseClass { get; set; }
        public DbSet<ControlAcceso.Models.HorseSaddle> HorseSaddle { get; set; }
        public DbSet<ControlAcceso.Models.RaceTrack> RaceTrack { get; set; }
        public DbSet<ControlAcceso.Models.Teacher> Teacher { get; set; }

    }
}
