﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Dtos
{
    public class CustomerDto
    {
        public int ID { get; set; }

        [Required]
        [StringLength(24, MinimumLength = 4, ErrorMessage = "el usuario debe de ser entre 4 y 25 de largo")]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public bool Active { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        [StringLength(3, ErrorMessage = "El nombre debe ser como mínimo 3 caracteres")]
        public string Name { get; set; }

        [Display(Name = "Apellidos")]
        [Required]
        [StringLength(10, ErrorMessage = "El apellido debe ser como mínimo 10 caracteres")]
        public string Surname { get; set; }

        [Required]
        public string Phone { get; set; }
        [Range(18, 65, ErrorMessage = "Tienes que ser mayor de edad!")]
        public int Age { get; set; }

        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
        [Required]
        [RegularExpression(@"^[0-9]{8}[A-Z]{1}$", ErrorMessage = "No es valido!")]
        public string DNI { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string PostalCode { get; set; }
        [Required]
        public string Level { get; set; }
    }
}
