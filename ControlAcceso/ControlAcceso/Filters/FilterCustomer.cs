﻿using ControlAcceso.Data;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Filters
{
    public class FilterCustomer : IActionFilter
    {
        private readonly ControlContext _context;
        public void OnActionExecuted(ActionExecutedContext context)
        {
            throw new NotImplementedException();
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var user = _context.UserAccounts.Where(u => u.ID == Convert.ToInt32(context.HttpContext.Session.GetString("usuario"))).FirstOrDefault();
            if (user == null)
                context.Result = new RedirectToActionResult("Index", "Login", null);

            var role = _context.Role.Where(r => r.ID == user.RoleID).FirstOrDefault();
            if (role == null)
            {
                context.Result = new RedirectToActionResult("Index", "Login", null);

            }
            else if (!role.RoleName.ToUpper().Equals("CUSTOMER"))
            {
                context.Result = new RedirectToActionResult("index", "login", null);
            }

            throw new NotImplementedException();
        }
    }
}
