﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Models
{
    public class Booking
    {
        public int ID { get; set; }
        public DateTime BookingDate { get; set; }
        public int CustomerID { get; set; }
        [ForeignKey("CustomerID")]
        public Customer Customer { get; set; }
        public int HorseID { get; set; }
        [ForeignKey("HorseID")]
        public Horse Horse { get; set; }
        public int HorseClassID { get; set; }
        [ForeignKey("HorseClassID")]
        public HorseClass HorseClass { get; set; }

    }
}
