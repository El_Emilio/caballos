﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Models
{
    public class Customer:Person
    {
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
        [Required]
        [RegularExpression(@"^[0-9]{8}[A-Z]{1}$", ErrorMessage = "No es valido!")]
        public string DNI { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public string PostalCode { get; set; }
        [Required]
        public string Level { get; set; }

        [InverseProperty("Customer")]
        public List<Booking> ListBookings { get; set; }

    }
}
