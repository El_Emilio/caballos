﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Models
{
    public class Horse
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Race { get; set; }
        [DataType(DataType.Date)]
        public DateTime Birthday { get; set; }
        [Required]
        public string Colour { get; set; }
        [Required]
        public string Level { get; set; }
        public int HorseSaddleID { get; set; }
        [ForeignKey("HorseSaddleID")]
        public HorseSaddle HorseSaddle { get; set; }
        [InverseProperty("Horse")]
        public List<Booking> ListBookings { get; set; }

        public byte[] Image { get; set; }
        public string ImageSourceFileName { get; set; }
        public string ImageContentType { get; set; }

    }
}
