﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Models
{
    public class HorseSaddle
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Condition { get; set; }
        public Boolean Reserved { get; set; }

    }
}
