﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Models
{
     public abstract class Person
    {
        public int ID { get; set; }

        [Display(Name = "Nombre")]
        [Required]
        [StringLength(20, ErrorMessage = "el nombre debe ser como mínimo 20 characters")]
        public string Name { get; set; }

        [Display(Name = "Apellidos")]
        [Required]
        [StringLength(20, ErrorMessage = "el apellido debe ser como mínimo 20 characters")]
        public string Surname { get; set; }

        [Required]
        public string Phone { get; set; }
        [Range(18, 65, ErrorMessage = "Debes ser mayor de edad")]
        public int Age { get; set; }

        public int UserAccountID { get; set; }
        [ForeignKey("UserAccountID")]
        public UserAccount UserAccount { get; set; }


    }
}
