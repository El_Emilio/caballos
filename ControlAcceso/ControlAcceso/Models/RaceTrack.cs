﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Models
{
    public class RaceTrack
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [InverseProperty("RaceTrack")]
        public List<HorseClass> ListHorseClass { get; set; }

    }
}
