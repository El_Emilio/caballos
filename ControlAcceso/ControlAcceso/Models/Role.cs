﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Models
{
    public class Role
    {
        public int ID { get; set; }

        [Display(Name = "Nombre del Rol")]
        [Required]
        public string RoleName { get; set; }

        [Display(Name = "Description")]
        [Required]
        [StringLength(100, ErrorMessage = "La descripción del rol no puede ser más largo que 100 cadénas de caractéres")]
        public string RoleDescription { get; set; }
        public bool Enabled { get; set; }

        [InverseProperty("Role")]
        public List<UserAccount> ListUserAccounts { get; set; }
        [InverseProperty("Role")]
        public List<RoleHasMenu> ListRolesHasMenu { get; set; }

    }
}
