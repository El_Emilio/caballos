﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ControlAcceso.Models
{
    public class Util
    {
        public static List<Menu> Menus { get; set; } = new List<Menu>();
        public static string Encriptar(string clave)
        {
            string result;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(clave);

            result = Convert.ToBase64String(encryted);
            return result;

        }
        public static byte[] GetByteArrayFromImage(IFormFile file)
        {
            using(var target=new MemoryStream())
            {
                file.CopyTo(target);
                return target.ToArray();
            }
        }
       


    }
}
